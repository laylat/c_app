== README

* Bitbucket link: https://bitbucket.org/laylat/c_app
* Heroku link: https://intense-refuge-71884.herokuapp.com/

* Heroku git URL: https://git.heroku.com/intense-refuge-71884.git


* Features: 
*           Can add, remove and edit activities
*           Form validation for grades to avoid null values, avoid marks less than 0, avoid totals equal or less than 0
*           Link to study tips

* Design decisions:
*           Due to added functionality to add/remove activities, placed average and mean buttons in center to align better with percentage column
*           Activities are enumerated by object id (instead of with a counter) to avoid confusion to the user

* Notes: created grades object using scaffold

Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.
