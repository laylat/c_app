class Grade < ActiveRecord::Base
      validates :Mark, presence: true, allow_nil: false, numericality: {greater_than_or_equal_to: 0 }
      validates :MarkOutOf, presence: true, allow_nil: false, numericality: {greater_than: 0 }
end
