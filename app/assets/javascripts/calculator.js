
var countActivities;
var meanOfGrades;
var averageOfGradesNumerator;
var averageOfGradesDenominator;
var averageOfGrades;

function AVERAGE() {
    document.getElementById("result").innerHTML = percentageString(averageOfGrades);
    //String(Math.round(10*averageOfGrades)/10).concat("%");
}

function MEAN() {
    document.getElementById("result").innerHTML = percentageString(meanOfGrades);
    //String(Math.round(10*meanOfGrades)/10).concat("%");
}

function getPercentage(numerator,denominator) {
    if (denominator > 0) {
        return 100*numerator/denominator;
    }
    else {
        return 0;
    }
}

function percentageString(percentage) {
    return String(Math.round(10*percentage)/10).concat("%");
}